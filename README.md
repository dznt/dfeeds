# DFEEDS

```
let feed = require('dfeeds');

let topic = new Uint8Array(32);
console.debug(new feed.indexed(topic).next());

> Uint8Array(32) [
>   173,  50,  40, 182, 118, 247, 211, 205,
>    66, 132, 165,  68,  63,  23, 241, 150,
>    43,  54, 228, 145, 179,  10,  64, 178,
>    64,  88,  73, 229, 151, 186,  95, 181
> ]
```
